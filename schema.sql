CREATE TABLE `RateLimitTracking`
(
    `RateLimitTrackingId` int(11) UNSIGNED NOT NULL,
    `SaslUsername`        varchar(255)     NOT NULL,
    `CurrentCounter`      decimal(12, 4)   NOT NULL,
    `SentCounter`         int(11) UNSIGNED NOT NULL,
    `RateLimit`           int(11)          NOT NULL,
    `LastUpdateTimestamp` bigint(20)       NOT NULL
) ENGINE = InnoDB
  DEFAULT CHARSET = utf8;

ALTER TABLE `RateLimitTracking`
    ADD PRIMARY KEY (`RateLimitTrackingId`),
    ADD UNIQUE KEY `SaslUsername` (`SaslUsername`),
    ADD KEY `LastUpdateTimestamp` (`LastUpdateTimestamp`);

ALTER TABLE `RateLimitTracking`
    MODIFY `RateLimitTrackingId` int(11) NOT NULL AUTO_INCREMENT;
