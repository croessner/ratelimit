package main

import (
	"encoding/json"
	"fmt"
	"io/ioutil"
	"log"
	"math/rand"
	"mime"
	"net/http"
	"os"
	"strconv"
	"strings"
	"time"
)

const rateLimitMax = 1000
const rateLimitMin = 300

type RequestData struct {
	Type  string `json:"type"`
	Email string `json:"email"`
}
type HttpRequest struct {
	Policy []RequestData `json:"policy"`
}

type HttpResponse struct {
	Type      string `json:"type"`
	Email     string `json:"email"`
	RateLimit int    `json:"ratelimit"`
}

type HttpApp struct{}

var fixedRateLimit = -1

func HasContentType(request *http.Request, mimetype string) bool {
	contentType := request.Header.Get("Content-type")
	for _, v := range strings.Split(contentType, ",") {
		t, _, err := mime.ParseMediaType(v)
		if err != nil {
			break
		}
		if t == mimetype {
			return true
		}
	}
	return false
}

func (a *HttpApp) httpRootPage(rw http.ResponseWriter, request *http.Request) {
	method := request.Method
	uri := request.URL

	switch method {
	case "POST":
		switch uri.Path {
		case "/sasl_username":
			var randomRate int

			if !HasContentType(request, "application/json") {
				rw.WriteHeader(http.StatusBadRequest)
				return
			}

			body, err := ioutil.ReadAll(request.Body)
			if err != nil {
				rw.WriteHeader(http.StatusInternalServerError)
				return
			}

			requestData := new(HttpRequest)
			if err := json.Unmarshal(body, requestData); err != nil {
				rw.WriteHeader(http.StatusBadRequest)
				return
			}

			// Sample ratelimit
			if fixedRateLimit > 0 {
				randomRate = fixedRateLimit
			} else {
				randomRate = rand.Intn(rateLimitMax-rateLimitMin+1) + rateLimitMin
			}
			log.Printf("Request: POST %+v rate=%d\n", requestData, randomRate)

			if len(requestData.Policy) != 1 {
				rw.WriteHeader(http.StatusBadRequest)
				return
			}
			if requestData.Policy[0].Type == "ratelimit" {
				if requestData.Policy[0].Email != "" {
					rs := []HttpResponse{{Type: "ratelimit", Email: requestData.Policy[0].Email, RateLimit: randomRate}}
					rw.Header().Set("Content-Type", "application/json")
					rw.WriteHeader(http.StatusAccepted)
					_ = json.NewEncoder(rw).Encode(rs)
					log.Printf("Response: %+v\n", rs)
				} else {
					rw.WriteHeader(http.StatusBadRequest)
				}
			} else {
				rw.WriteHeader(http.StatusBadRequest)
			}

		default:
			rw.WriteHeader(http.StatusNotFound)
		}
	}
}

func main() {
	if len(os.Args) == 2 {
		rate, err := strconv.Atoi(os.Args[1])
		if err != nil {
			log.Fatalln(err)
		} else {
			fixedRateLimit = rate
		}
	}
	rand.Seed(time.Now().UnixNano())

	httpApp()
}

func httpApp() {
	var err error
	app := &HttpApp{}

	mux := http.NewServeMux()
	mux.HandleFunc("/", app.httpRootPage)

	www := &http.Server{
		Addr:         "0.0.0.0:10080",
		Handler:      mux,
		IdleTimeout:  time.Minute,
		ReadTimeout:  10 * time.Second,
		WriteTimeout: 30 * time.Second,
	}

	err = www.ListenAndServe()
	fmt.Println(err)
}
