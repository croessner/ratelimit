/*
ratelimit
Copyright 2021 Rößner-Network-Solutions

Redistribution and use in source and binary forms, with or without modification, are permitted provided that the
following conditions are met:

1. Redistributions of source code must retain the above copyright notice, this list of conditions and the following
disclaimer.

2. Redistributions in binary form must reproduce the above copyright notice, this list of conditions and the following
disclaimer in the documentation and/or other materials provided with the distribution.

THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES,
INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY,
WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF
THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
*/

package main

import (
	"database/sql"
	"fmt"
	sq "github.com/Masterminds/squirrel"
	_ "github.com/go-sql-driver/mysql"
	"reflect"
)

const idleMySQLConnections = 5

type Database struct {
	Username  string
	Password  string
	Hostname  string
	Port      int
	Database  string
	TableName string

	MaxOpenConnections int
	DebugSQL           bool

	Conn *sql.DB
}

func (db *Database) String() string {
	var result string

	v := reflect.ValueOf(*db)
	typeOfc := v.Type()

	for i := 0; i < v.NumField(); i++ {
		switch typeOfc.Field(i).Name {
		case "Password", "Conn":
			continue
		default:
			result += fmt.Sprintf(" %s='%v'", typeOfc.Field(i).Name, v.Field(i).Interface())
		}
	}

	return result[1:]
}

func (db *Database) connect() {
	var err error

	db.Conn, err = sql.Open("mysql", fmt.Sprintf("%s:%s@tcp(%s:%d)/%s",
		db.Username, db.Password, db.Hostname, db.Port, db.Database))
	if err != nil {
		ErrorLogger.Fatalln(err)
	}

	db.Conn.SetMaxOpenConns(db.MaxOpenConnections)
	db.Conn.SetMaxIdleConns(idleMySQLConnections)
}

func (db *Database) search(sqlStatement sq.SelectBuilder, t interface{}, instance string) error {
	if db.DebugSQL {
		statement, val, _ := sqlStatement.ToSql()
		DebugLogger.Printf("guid=\"%s\" sql=\"%s\" values=\"%s\"\n", instance, statement, val)
	}
	rows, err := sqlStatement.RunWith(db.Conn).Query()
	if err != nil {
		return err
	}

	//goland:noinspection GoUnhandledErrorResult
	defer rows.Close()

	for rows.Next() {
		switch t.(type) {
		case *RateLimitTable:
			tbl := t.(*RateLimitTable)
			err = rows.Scan(&tbl.SaslUsername, &tbl.CurrentCounter, &tbl.SentCounter, &tbl.RateLimit, &tbl.LastUpdateTimestamp, &tbl.UnixTimestamp)
			if err != nil {
				return err
			}
		case *UnixTimestampTable:
			tbl := t.(*UnixTimestampTable)
			err = rows.Scan(&tbl.UnixTimestamp)
			if err != nil {
				return err
			}
		}
		break
	}

	return nil
}

func (db *Database) insert(sqlStatement sq.InsertBuilder, instance string) error {
	if db.DebugSQL {
		statement, val, _ := sqlStatement.ToSql()
		DebugLogger.Printf("guid=\"%s\" sql=\"%s\" values=\"%s\"\n", instance, statement, val)
	}
	rows, err := sqlStatement.RunWith(db.Conn).Query()

	if err != nil {
		return err
	}

	//goland:noinspection GoUnhandledErrorResult
	defer rows.Close()

	return nil
}

func (db *Database) update(sqlStatement sq.UpdateBuilder, instance string) error {
	if db.DebugSQL {
		statement, val, _ := sqlStatement.ToSql()
		DebugLogger.Printf("guid=\"%s\" sql=\"%s\" values=\"%s\"\n", instance, statement, val)
	}
	rows, err := sqlStatement.RunWith(db.Conn).Query()

	if err != nil {
		return err
	}

	//goland:noinspection GoUnhandledErrorResult
	defer rows.Close()

	return nil
}
