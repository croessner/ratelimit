FROM golang:1.17-alpine AS builder

WORKDIR /build

# Copy and download dependencies using go.mod
COPY . ./
RUN go mod download

# Set necessarry environment vairables and compile the app
ENV CGO_ENABLED=0 GOOS=linux GOARCH=amd64
RUN go build -v -ldflags="-s -w" -o ratelimit .
RUN cd ./stresstest && go build -v -v -ldflags="-s -w" -o stresstest main.go
RUN cd ./samplehttp && go build -v -v -ldflags="-s -w" -o samplehttp main.go

FROM scratch

LABEL org.opencontainers.image.authors="christian@roessner.email"
LABEL com.roessner-network-solutions.vendor="Rößner-Network-Solutions"
LABEL version="@@gittag@@-@@gitcommit@@"
LABEL description="Postfix policy service that blocks clients, if their daily sender rate limit is over quota."

WORKDIR /usr/app

# Copy binary to destination image
COPY --from=builder ["/build/ratelimit", "./"]
COPY --from=builder ["/build/stresstest/stresstest", "./"]
COPY --from=builder ["/build/samplehttp/samplehttp", "./"]

EXPOSE 4647

CMD ["/usr/app/ratelimit", "server"]
