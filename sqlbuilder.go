/*
ratelimit
Copyright 2021 Rößner-Network-Solutions

Redistribution and use in source and binary forms, with or without modification, are permitted provided that the
following conditions are met:

1. Redistributions of source code must retain the above copyright notice, this list of conditions and the following
disclaimer.

2. Redistributions in binary form must reproduce the above copyright notice, this list of conditions and the following
disclaimer in the documentation and/or other materials provided with the distribution.

THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES,
INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY,
WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF
THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
*/

package main

import (
	sq "github.com/Masterminds/squirrel"
)

const (
	SaslUsername        = "SaslUsername"
	CurrentCounter      = "CurrentCounter"
	SentCounter         = "SentCounter"
	RateLimit           = "RateLimit"
	LastUpdateTimeStamp = "LastUpdateTimestamp"
)

// Duplicate entry 'X' for key 'Y'
const duplicateKey uint16 = 1062

type RateLimitTable struct {
	SaslUsername        string  `json:"SaslUsername"`
	CurrentCounter      float32 `json:"CurrentCounter"`
	SentCounter         uint32  `json:"SentCounter"`
	RateLimit           int32   `json:"RateLimit"`
	LastUpdateTimestamp int64   `json:"LastUpdateTimestamp"`
	UnixTimestamp       int64   `json:"UnixTimestamp"`
}

type UnixTimestampTable struct {
	UnixTimestamp int64 `json:"UnixTimestamp"`
}

func (rl *RateLimitTable) getCurrentRow(sender string, db *Database, instance string) error {
	s := sq.Select(SaslUsername, CurrentCounter, SentCounter, RateLimit, LastUpdateTimeStamp, "UNIX_TIMESTAMP() AS UnixTimestamp")
	s = s.From(db.TableName)
	s = s.Where(sq.Eq{SaslUsername: sender}).Limit(1)

	err := db.search(s, rl, instance)
	if err != nil {
		return err
	}

	return nil
}

func (rl *RateLimitTable) setCurrentRow(db *Database, instance string) error {
	s := sq.Insert(db.TableName)
	s = s.Columns(SaslUsername, CurrentCounter, SentCounter, RateLimit, LastUpdateTimeStamp)
	s = s.Values(rl.SaslUsername, rl.CurrentCounter, rl.SentCounter, rl.RateLimit, rl.LastUpdateTimestamp)

	err := db.insert(s, instance)
	if err != nil {
		return err
	}

	return nil
}

func (rl *RateLimitTable) updateCurrentRow(sender string, db *Database, updateRateLimitValue bool, instance string) error {
	s := sq.Update(db.TableName)
	s = s.Set(CurrentCounter, rl.CurrentCounter)
	s = s.Set(SentCounter, rl.SentCounter)
	if updateRateLimitValue {
		s = s.Set(RateLimit, rl.RateLimit)
	}
	s = s.Set(LastUpdateTimeStamp, rl.LastUpdateTimestamp)
	s = s.Where(sq.Eq{SaslUsername: sender})

	err := db.update(s, instance)
	if err != nil {
		return err
	}

	return nil
}

func (u *UnixTimestampTable) getUnixTimestamp(db *Database, instance string) error {
	s := sq.Select("UNIX_TIMESTAMP() AS UnixTimestamp")

	err := db.search(s, u, instance)
	if err != nil {
		return err
	}

	return nil
}
