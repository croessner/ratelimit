/*
ratelimit
Copyright 2021 Rößner-Network-Solutions

Redistribution and use in source and binary forms, with or without modification, are permitted provided that the
following conditions are met:

1. Redistributions of source code must retain the above copyright notice, this list of conditions and the following
disclaimer.

2. Redistributions in binary form must reproduce the above copyright notice, this list of conditions and the following
disclaimer in the documentation and/or other materials provided with the distribution.

THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES,
INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY,
WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF
THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
*/

package main

import (
	"fmt"
	"github.com/go-sql-driver/mysql"
	"strconv"
	"time"
)

// Defaults
const deferText = "DEFER Service temporarily not available"
const rejectText = "REJECT Your sender rate is over limits"

type Policy struct {
	policyRequest map[string]string

	queueId        string
	guid           string
	sender         string
	recipientCount int
	rateLimit      int32
	currentCounter float32
	sentCounter    uint32
}

func (p *Policy) IsOverLimit(cfg *CmdLineConfig, db *Database) (bool, error) {
	var ok bool
	var request string
	reject := false
	rl := &RateLimitTable{}

	if request, ok = p.policyRequest["request"]; ok {
		if request == "smtpd_access_policy" {
			userAttribute := "sender"
			if cfg.UseSASLUsername {
				userAttribute = "sasl_username"
			}
			if p.sender, ok = p.policyRequest[userAttribute]; ok {
				if len(p.sender) > 0 {
					if protoState, ok := p.policyRequest["protocol_state"]; ok {
						if protoState == "END-OF-MESSAGE" {
							if recipientCountStr, ok := p.policyRequest["recipient_count"]; ok {
								var err error

								if qid, ok := p.policyRequest["queue_id"]; ok {
									p.queueId = qid
								}

								p.recipientCount, err = strconv.Atoi(recipientCountStr)
								if err != nil {
									return false, err
								}
								if p.recipientCount == 0 {
									return false, fmt.Errorf("recipient_count == 0")
								}

								err = rl.getCurrentRow(p.sender, db, p.guid)
								if err != nil {
									return false, err
								}

								updateRateLimitValue := false
								if rl.RateLimit == 0 {
									// No existing record for the current user

									// Initialize record
									rl.SaslUsername = p.sender
									if cfg.UseHTTP {
										rl.RateLimit, err = getRateLimitHttp(p.sender, cfg.HttpAddress, cfg.VerboseLevel, p.guid)
									} else if cfg.UseLDAP {
										rl.RateLimit, err = getRateLimitLdap(p.sender, cfg.VerboseLevel, p.guid)
									} else {
										return false, fmt.Errorf("need HTTP or LDAP")
									}
									if err != nil {
										return false, err
									}
									ut := new(UnixTimestampTable)
									err = ut.getUnixTimestamp(db, p.guid)
									if err != nil {
										return false, err
									}
									if int32(p.recipientCount) <= rl.RateLimit {
										rl.CurrentCounter = float32(p.recipientCount)
										rl.SentCounter = uint32(p.recipientCount)
									} else {
										reject = true
									}
									rl.LastUpdateTimestamp = ut.UnixTimestamp
									rl.UnixTimestamp = ut.UnixTimestamp

									err = rl.setCurrentRow(db, p.guid)
									if err != nil {
										if mysqlErr, ok := err.(*mysql.MySQLError); ok {
											if mysqlErr.Number == duplicateKey {
												err := rl.updateCurrentRow(p.sender, db, false, p.guid)
												if err != nil {
													return false, err
												}
											} else {
												return false, err
											}
										} else {
											return false, err
										}
									}
								} else {
									// User already exists

									l := time.Unix(rl.LastUpdateTimestamp, 0).UTC()
									c := time.Unix(rl.UnixTimestamp, 0).UTC()
									midnightL := time.Date(l.Year(), l.Month(), l.Day(), 0, 0, 0, 0, time.UTC)
									midnightC := time.Date(c.Year(), c.Month(), c.Day(), 0, 0, 0, 0, time.UTC)
									if !midnightL.Equal(midnightC) {
										// New first mail next day
										if cfg.UseHTTP {
											rl.RateLimit, err = getRateLimitHttp(p.sender, cfg.HttpAddress, cfg.VerboseLevel, p.guid)
										} else if cfg.UseLDAP {
											rl.RateLimit, err = getRateLimitLdap(p.sender, cfg.VerboseLevel, p.guid)
										} else {
											return false, fmt.Errorf("need HTTP or LDAP")
										}
										if err != nil {
											return false, err
										}
										updateRateLimitValue = true
										rl.SentCounter = 0
									}

									timeDelta := rl.UnixTimestamp - rl.LastUpdateTimestamp
									if cfg.VerboseLevel == logLevelDebug {
										DebugLogger.Printf("guid=\"%s\" Unix_Timestamp() - LastUpdateTimestamp: timeDelta=%d\n", p.guid, timeDelta)
									}
									bonus := float32(rl.RateLimit) / 86400.0 * float32(timeDelta)
									if rl.CurrentCounter >= bonus {
										rl.CurrentCounter -= bonus
									} else {
										rl.CurrentCounter = 0
									}
									rl.LastUpdateTimestamp = rl.UnixTimestamp
									if rl.CurrentCounter <= float32(rl.RateLimit-int32(p.recipientCount)) {
										rl.CurrentCounter += float32(p.recipientCount)
										rl.SentCounter += uint32(p.recipientCount)
									} else {
										reject = true
									}

									err = rl.updateCurrentRow(p.sender, db, updateRateLimitValue, p.guid)
									if err != nil {
										return false, err
									}
								}

								if cfg.VerboseLevel == logLevelDebug {
									DebugLogger.Printf("guid=\"%s\" %+v\n", p.guid, rl)
								}

								p.rateLimit = rl.RateLimit
								p.currentCounter = rl.CurrentCounter
								p.sentCounter = rl.SentCounter
							}
						} else {
							return false, fmt.Errorf("protocol_state != END-OF-MESSAGE: found protocol_state: %s", protoState)
						}
					}
				}
			}
		}
	}

	return reject, nil
}
