/*
ratelimit
Copyright 2021 Rößner-Network-Solutions

Redistribution and use in source and binary forms, with or without modification, are permitted provided that the
following conditions are met:

1. Redistributions of source code must retain the above copyright notice, this list of conditions and the following
disclaimer.

2. Redistributions in binary form must reproduce the above copyright notice, this list of conditions and the following
disclaimer in the documentation and/or other materials provided with the distribution.

THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES,
INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY,
WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF
THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
*/

package main

import (
	"bufio"
	"fmt"
	"github.com/segmentio/ksuid"
	"net"
	"strings"
)

func clientConnections(listener net.Listener, cfg *CmdLineConfig) chan net.Conn {
	ch := make(chan net.Conn)
	go func() {
		for {
			client, err := listener.Accept()
			if client == nil {
				ErrorLogger.Println("Couldn't accept connection:", err)
				continue
			}
			if cfg.VerboseLevel == logLevelDebug {
				DebugLogger.Printf("Client %v connected\n", client.RemoteAddr())
			}
			ch <- client
		}
	}()
	return ch
}

//goland:noinspection GoUnhandledErrorResult
func handleConnection(client net.Conn, cfg *CmdLineConfig, db *Database) {
	b := bufio.NewReader(client)
	var policyRequest = make(map[string]string)

	for {
		lineBytes, err := b.ReadBytes('\n')
		if err != nil { // EOF, or worse
			if cfg.VerboseLevel == logLevelDebug {
				DebugLogger.Printf("Client %v disconnected\n", client.RemoteAddr())
			}
			client.Close()
			break
		}

		lineStr := strings.TrimSpace(string(lineBytes))
		items := strings.SplitN(lineStr, "=", 2)
		if len(items) == 2 {
			policyRequest[strings.TrimSpace(items[0])] = strings.TrimSpace(items[1])
		} else {
			var (
				sender        string
				recipient     string
				size          string
				clientAddress string
				tlsProtocol   string
				tlsCipher     string
			)

			failed := false
			actionText := "DUNNO"

			if val, ok := policyRequest["recipient"]; ok {
				recipient = val
			}
			if val, ok := policyRequest["size"]; ok {
				size = val
			}
			if val, ok := policyRequest["client_address"]; ok {
				clientAddress = val
			}
			if val, ok := policyRequest["encryption_protocol"]; ok {
				tlsProtocol = val
			}
			if val, ok := policyRequest["encryption_cipher"]; ok {
				tlsCipher = val
			}

			if recipient == "" {
				recipient = "-"
			}
			if size == "" {
				size = "0"
			}
			if clientAddress == "" {
				clientAddress = "-"
			}
			if tlsProtocol == "" {
				tlsProtocol = "-"
			}
			if tlsCipher == "" {
				tlsCipher = "-"
			}

			p := &Policy{policyRequest: policyRequest}
			p.guid = ksuid.New().String()

			if cfg.VerboseLevel == logLevelDebug {
				DebugLogger.Printf("guid=\"%s\" %+v\n", p.guid, policyRequest)
			}
			overLimit, err := p.IsOverLimit(cfg, db)
			if err != nil {
				failed = true
				ErrorLogger.Println(err)
				actionText = cfg.DeferText
			} else {
				if overLimit {
					actionText = cfg.RejectText
				}
			}

			if cfg.UseSASLUsername {
				sender = fmt.Sprintf("sasl_username=\"%s\"", p.sender)
			} else {
				sender = fmt.Sprintf("sender=\"<%s>\"", p.sender)
			}

			if cfg.VerboseLevel >= logLevelInfo {
				InfoLogger.Printf("guid=\"%s\" qid=\"%s\" client_address=\"%s\" %s tls_protocol=\"%s\" tls_cipher=\"%s\" recipient_count=%d recipient=\"%s\" rate_limit=%d current_counter=%.4f sent_counter=%d size=%s errors=\"%v\" action=\"%s\"\n",
					p.guid, p.queueId, clientAddress, sender, tlsProtocol, tlsCipher, p.recipientCount, recipient, p.rateLimit, p.currentCounter, p.sentCounter, size, failed, actionText)
			}

			client.Write([]byte(fmt.Sprintf("action=%s\n\n", actionText)))
			policyRequest = make(map[string]string) // Clear policy request for next connection
		}
	}
}
