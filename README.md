# ratelimit

## About

Postfix-Submission policy server that enforces a sender rate limit. It currently gets the rate limit for
a user from a HTTP web server using a POST-request. A MySQL/MariaDB SQL server is used to track the
current sender rate. The rate is calculated as a leaky bucket.

## Features

* Receive rate limit from HTTP server
* Reject a sender, if the sender rate is over limit

# Table of contents

1. [Install](#install)
   * [Postfix integration](#postfix-integration)
   * [Preparing a docker image](#preparing-a-docker-image)
   * [Build from source](#build-from-source)
   * [Server options](#server-options)
2. [Environment variables](#environment-variables)
   * [Server](#server)
3. [HTTP request](#http-request)

# Install

## Postfix integration

The service is configured in Postfix like this...

```
smtpd_end_of_data_restrictions =
    ...
    check_policy_service inet:127.0.0.1:4647
    ...
```

... if you use the docker-compose.yml file as provided.

Back to [table of contents](#table-of-contents)

## Preparing a docker image

The simplest way to use the program is by using a docker image.

```shell
cd /path/to/Dockerfile
docker build -t ratelimit:latest .
```

Back to [table of contents](#table-of-contents)

## Build from source

### Requirements

To build ratelimit from source, you need at least Golang version 1.16 or above.

### Compile

Build the main application:

```shell
cd /path/to/sourcecode
go mod tidy
go build -o ratelimit .
```

Build the **samplehttp** server:

```shell
cd /path/to/sourcecode/samplehttp
go build -o samplehttp main.go
```

Build the **stresstest** binary:
```shell
cd /path/to/sourcecode/stresstest
go build -o stresstest main.go
````

You can find an example systemd unit file as well as a default configuration file in the contrib folder.

Back to [table of contents](#table-of-contents)

## Server options

```shell
ratelimit server --help
```

produces the following output:

```
...

Arguments:

  -h  --help                     Print help information
  -a  --server-address           IPv4 or IPv6 address for the policy service. Default: 127.0.0.1
  -p  --server-port              Port for the policy service. Default: 4647
      --use-http                 Enable HTTP support. Default: false
      --http-address             HTTP address for the HTTP username endpoint. Default: http://127.0.0.1:10080/sasl_username
      --defer-text               Postfix access(5) reply message that is sent if the service is temporarily broken. Default: DEFER Service temporarily not available
      --reject-text              Postfix access(5) reply message that is sent if the sender rate was over limit. Default: REJECT Your sender rate is over limits
      --db-username              Database username. Default: ratelimit
      --db-password              Database password. Default: ratelimit
      --db-hostname              Database hostname or IP address. Default: mariadb
      --db-port                  Database port number. Default: 3306
      --db-name                  Database name. Default: ratelimit
      --db-table-name            Database table name. Default: RateLimitTracking
      --db-max-open-connections  Database maximum open connections. Default: 10
      --db-debug-sql             Print out rendered SQL statements. Default: false
      --use-ldap                 Enable LDAP support. Default: false
      --ldap-server-uri          Server URI. Specify multiple times, if you need more than one server. Default: [ldap://127.0.0.1:389/]
      --ldap-basedn              Base DN. Default: 
      --ldap-binddn              Bind DN. Default: 
      --ldap-bindpw              Bind password. Default: 
      --ldap-filter              Filter with %s placeholder. Default: (&(objectClass=*)(mailAlias=%s))
      --ldap-result-attribute    Result attribute for the requested mail sender. Default: mailAccount
      --ldap-starttls            If this option is given, use StartTLS. Default: false
      --ldap-skip-tls-verify     Skip TLS server name verification. Default: false
      --ldap-tls-cafile          File containing TLS CA certificate(s). Default: 
      --ldap-tls-client-cert     File containing a TLS client certificate. Default: 
      --ldap-tls-client-key      File containing a TLS client key. Default: 
      --ldap-sasl-external       Use SASL/EXTERNAL instead of a simple bind. Default: false
      --ldap-scope               LDAP search scope [base, one, sub]. Default: sub
      --use-sasl-username        Use 'sasl_username' instead of the 'sender' attribute. Default: false
  -v  --verbose                  Verbose mode. Repeat this for an increased log level
      --version                  Current version
```

Back to [table of contents](#table-of-contents)

# Environment variables

The following environment variables can be used to configure the policy service. This is especially useful, if you plan
on running the service as a docker service.

## Server

Variable | Description
---|---
SERVER_ADDRESS | IPv4 or IPv6 address for the policy service; default(127.0.0.1)
SERVER_PORT | Port for the policy service; default(4647)
USE_HTTP | Enable HTTP support
HTTP_ADDRESS | HTTP address for the HTTP username endpoint; default(http://127.0.0.1:10080/sasl_username)
DEFER_TEXT | Postfix access(5) reply message that is sent if the service is temporarily broken; default(DEFER Service temporarily not available)
REJECT_TEXT | Postfix access(5) reply message that is sent if the sender rate was over limit; default(REJECT Your sender rate is over limits)
DB_USERNAME | Database username; default(ratelimit)
DB_PASSWORD | Database password; default(ratelimit)
DB_HOSTNAME | Database hostname or IP address; default(mariadb)
DB_PORT | Database port number; default(3306)
DB_NAME | Database name; default(ratelimit)
DB_TABLE_NAME | Database table name; default(RateLimitTracking)
DB_MAX_OPEN_CONNECTIONS | Database maximum open connections; default(10)
DB_DEBUG_SQL | Print out rendered SQL statements; default(false)
USE_LDAP | Enable LDAP support; default(false)
LDAP_SERVER_URIS | Server URI. Specify multiple times, if you need more than one server; default(ldap://127.0.0.1:389/)
LDAP_BASEDN | Base DN
LDAP_BINDPW | Bind PW
LDAP_FILTER | Filter with %s placeholder; default( (&(objectClass=*)(mailAlias=%s)) )
LDAP_RESULT_ATTRIBUTE | Result attribute for the requested mail sender; default(mailAccount)
LDAP_STARTTLS | If this option is given, use StartTLS
LDAP_TLS_SKIP_VERIFY | Skip TLS server name verification
LDAP_TLS_CAFILE | File containing TLS CA certificate(s)
LDAP_TLS_CLIENT_CERT | File containing a TLS client certificate
LDAP_TLS_CLIENT_KEY | File containing a TLS client key
LDAP_SASL_EXTERNAL | Use SASL/EXTERNAL instead of a simple bind; default(false)
LDAP_SCOPE | LDAP search scope [base, one, sub]; default(sub)
USE_SASL_USERNAME | Use 'sasl_username' instead of the 'sender' attribute; default(false)
VERBOSE | Log level. One of 'none', 'info' or 'debug'

Back to [table of contents](#table-of-contents)

## HTTP request

The policy service sends a policy request like the following example demonstrates:

```shell
curl -i -X POST http://127.0.0.1:10080/sasl_username -H 'Content-Type: application/json' -d '{"policy":[{"type":"ratelimit", "email":"test@example.test"}]}'
```

The result mus be exactly like this:

```json
[{"type":"ratelimit","email":"test@example.test","ratelimit":500}]
```

If you do not have a HTTP server, you can simply pick the samplehttp server and run it like this:

```shell
./samplehttp 250
```

250 is a fixed rate limit. Chose whatever suits your needs. If you use the docker-compose.yml file, you may run things like this:

```
version: "3.8"

services:

  mariadb:
    image: mariadb:latest
    logging:
      driver: journald
      options:
        tag: mariadb_ratelimit
    restart: always
    environment:
      MYSQL_RANDOM_ROOT_PASSWORD: "yes"
      MYSQL_DATABASE: "ratelimit"
      MYSQL_USER: ${MYSQL_USER}
      MYSQL_PASSWORD: ${MYSQL_PASSWORD}
    command: ["--character-set-server=utf8mb4", "--collation-server=utf8mb4_unicode_ci"]
    volumes:
      - mysql:/var/lib/mysql:z

  samplehttp:
    image: ratelimit:latest
    logging:
      driver: journald
      options:
        tag: samplehttp
    restart: always
    command: ["/usr/app/samplehttp", "250"]
    depends_on:
      - mariadb
      
  ratelimit:
    image: ratelimit:latest
    logging:
      driver: journald
      options:
        tag: ratelimit
    restart: always
    environment:
      VERBOSE: "debug"
      SERVER_ADDRESS: "0.0.0.0"
      SERVER_PORT: "4647"
      HTTP_ADDRESS: http://samplehttp:10080/sasl_username
      USE_SASL_USERNAME: "true"
    ports:
      - "127.0.0.1:4647:4647"
    depends_on:
      - mariadb

volumes:
  mysql:
```

Back to [table of contents](#table-of-contents)

Hope you enjoy :-)